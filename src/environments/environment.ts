// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBEkKJEjZlHpf3OcNTFm5TaH8QmVhq-d7o",
    authDomain: "beni-angular.firebaseapp.com",
    databaseURL: "https://beni-angular.firebaseio.com",
    projectId: "beni-angular",
    storageBucket: "beni-angular.appspot.com",
    messagingSenderId: "860283989829",
    appId: "1:860283989829:web:84eb23e327a5e480e7982c",
    measurementId: "G-6HW5ZNP381"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
